%dw 2.0
output application/json

var accounts = vars.wexnetXMLResponse map(item, index) -> {
    "code":item.carrier
}

fun getCountryCode(inObj) = if(inObj == "USA")
"US"
else
inObj

var accessorialMapping = {
 "insidePickup": "INPU",
 "liftgatePickup": "LGPU",
 "residentialPickup": "RESPU",
 "tradeshowPickup": "CNVPU",
 "limitedAccessPickup": "LTDPU",
 "constructionSitePickup": "CONPU",
 "insideDelivery": "INDEL",
  "liftgateDelivery": "LGDEL",
  "residentialDelivery": "RESDEL",
  "tradeshowDelivery": "CNVDEL",
  "limitedAccessDelivery": "LTDDEL",
  "constructionSiteDelivery": "CONDEL"
}

var handlingUnitMapping = {
"BAL": "BALE",
"BAR": "DRUM",
"BBL": "DRUM",
"BDL": "BUNDLE",
"BEM": "PIECES",
"BIN": "BOX",
"BSK": "CRATE",
"BUN": "BUNDLE",
"BXT": "BUCKET",
"CAB": "BOX",
"CAR": "CRATE",
"CAS": "CASE",
"CBY": "CAN",
"CHS": "CRATE",
"CNT": "BOX",
"COL": "COIL",
"CON": "BUCKET",
"CRT": "CRATE",
"CSK": "DRUM",
"CTN": "CARTON",
"CYL": "CYLINDER",
"DRM": "DRUM",
"FIR": "DRUM",
"JAR": "CAN",
"KEG": "DRUM",
"KIT": "PIECES",
"LOO": "PIECES",
"LSE": "PIECES",
"OTH": "PIECES",
"PAL": "BUCKET",
"PCS": "PIECES",
"PKG": "PLT",
"PLN": "TUBE",
"REL": "REEL",
"ROL": "ROLL",
"SKD": "SKID",
"SLP": "PLT",
"SPL": "REEL",
"STK": "PLT",
"TBE": "TUBE",
"TBN": "CRATE",
"TNK": "DRUM",
"TRK": "CRATE",
"TTC": "CRATE",
"TUB": "BUCKET",
"UNT": "PIECES",
"VPK": "PIECES",
"WRP": "PIECES",
"PLT": "PLT",
"CAN": "CAN",
"BOX": "BOX",
"BAG": "BAG"
}
var accessorials = vars.wexnetDetails filterObject ((value, key, index) -> (key match{
 case "insidePickup" -> true
 case "liftgatePickup" -> true
 case "residentialPickup" -> true
 case "tradeshowPickup" -> true
 case "limitedAccessPickup" -> true
 case "constructionSitePickup" -> true
 case "insideDelivery" -> true
 case "liftgateDelivery" -> true
 case "residentialDelivery" -> true
 case "tradeshowDelivery" -> true
 case "limitedAccessDelivery" -> true
 case "constructionSiteDelivery" -> true
 else -> false
}) and value == "Y")  pluck ((value, key, index) -> {code:accessorialMapping[key]}) 

var isHazmat = vars.wexnetDetails.commdityDetails.handlingUnitDetails.wsHandlingUnit.lineItemDetails map ((item, index) -> 
item ) map ((itemz, index) -> itemz.wsLineItem.isHazmatLineItem default "" contains  "Y") contains  true

var hazCode = {
    code:"HAZM"
}

fun checkHazmat() = if(isHazmat){
	inOb : (accessorials default []) + hazCode
} else
{
    inOb : accessorials 
}
---
{
    "capacityProviderAccountGroup": {
        "code": "Default",
        "accounts": accounts
    },
   
    "originAddress": {
        "postalCode": vars.wexnetDetails.senderZip,
        "city": vars.wexnetDetails.senderCity,
        "state": vars.wexnetDetails.senderState,
        "country": getCountryCode(vars.wexnetDetails.senderCountryCode)
    },
    "destinationAddress": {
        "postalCode": vars.wexnetDetails.receiverZip,
        "city": vars.wexnetDetails.receiverCity,
        "state": vars.wexnetDetails.receiverState,
        "country": getCountryCode(vars.wexnetDetails.receiverCountryCode)
    },
    "lineItems": vars.wexnetDetails.commdityDetails.handlingUnitDetails.wsHandlingUnit map ((item, index) ->
        {
            "totalWeight": sum(item.lineItemDetails map ((litem, index) -> 
    			litem.wsLineItem.lineItemWeight
			    )) as String,
            "packageDimensions": {
                "length": item.handlingUnitLength,
                "width": item.handlingUnitWidth,
                "height": item.handlingUnitHeight
            },
            "freightClass": max(item.lineItemDetails map ((litem, index) -> 
		    	litem.wsLineItem.lineItemClass
			   )),
            "packageType": handlingUnitMapping[item.typeOfHandlingUnit],
            "totalPackages": item.numberOfHandlingUnit,
            "totalPieces": sum(item.lineItemDetails map ((litem, index) -> 
    			litem.wsLineItem.piecesOfLineItem
    			)) as String,
            "commodityType": item.lineItemDetails.wsLineItem[0].lineItemPieceType,
        }
    ),
"accessorialServices": checkHazmat().inOb,
    "preferredSystemOfMeasurement": "METRIC",
    "directionOverride": "THIRD_PARTY"
}