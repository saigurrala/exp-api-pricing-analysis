%dw 2.0
output application/json

fun comparePrice(obj1, obj2) = if(obj1 == obj2)
"Equal"
else
"Not Equal"

var normalPrice = payload groupBy ((item, index) -> 
item.carrier
) pluck ((value, key, index) -> 
{
	"bol":vars.wexnetDetails.bolNbr,
    carrier: key,
    wexnetPrice: value.wexnetPrice[0] as String,
    p44Price: value.p44price[0] as String,
    difference: comparePrice(value.wexnetPrice[0] as String, value.p44price[0] as String)
}
) filter ((item, index) -> 
item.wexnetPrice != 'N/A' or item.p44Price != 'N/A' 
)

var gurantedPrice = payload groupBy ((item, index) -> 
item.carrier
) pluck ((value, key, index) -> 
{
	"bol":vars.wexnetDetails.bolNbr,
    carrier: key++"G",
    wexnetGurantedPrice: value.wexnetGurantedPrice[0] as String ,
    p44GurantedPrice: value.p44GurrantedPrice[0] as String,
    difference: comparePrice(value.wexnetGurantedPrice[0] as String, value.p44GurrantedPrice[0] as String)

}
) filter ((item, index) -> 
item.wexnetGurantedPrice != 'N/A' or item.p44GurantedPrice != 'N/A'
)
---
{
    "bol":vars.wexnetDetails.bolNbr,
	"carrierPriceDetails":(normalPrice ++ gurantedPrice)
}