%dw 2.0
output application/json
---
payload.rateQuotes map ((item, index) -> {
"carrier":item.capacityProviderAccountGroup.accounts[0].code,
"p44price": item.rateQuoteDetail.total default "N/A" as String,
"p44GurrantedPrice": (item.alternateRateQuotes[?($.serviceLevel.code=="GUR")].rateQuoteDetail.total[0]) default "N/A" as String,

}
)