%dw 2.0
output application/json

var result = payload..*freightShipmentQuoteResult map ((item, index) -> {
"carrier": item.carrierSCAC,
"wexnetPrice": item.totalPrice,
"guranted": item.guaranteedService
}
)
---


result groupBy ((item, index) -> 
    item.carrier 
) pluck ((value, key, index) -> 
{
    carrier:key,
    wexnetPrice: value[?($.guranted == 'N')].wexnetPrice[0] default "N/A" as String,
    wexnetGurantedPrice: (value[?($.guranted == 'Y')].wexnetPrice[0]) default "N/A" as String
}
) 