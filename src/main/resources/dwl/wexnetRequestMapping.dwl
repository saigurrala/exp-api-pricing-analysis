%dw 2.0
output application/xml
ns ns0 http://www.wwexship.com
fun checkTypeOfHU(inObj) = 
if(["CYL","BAL","DRM","BAG","BOX","LSE","BDL","CTN","UNT","SKD","CRT","PAL","PLT","CBY","ROL","CYLINDER","BALE","DRUM","BAG","BOX","LOOSE","BUNDLE","CARTON","OTHER","SKID","CRATE","PAILS","PALLET","GAYLORD","ROLL"] contains(inObj))
inObj
else
"Other"
fun checkLineItemPieceType(inObj) = 
if(["BXT","TBE","CYL","BAL","BXT","BAG","DRM","BOX","TBN","PCS","CAS","BDL","CTN","COL","UNT","BXT","CAN","CRT","CBY","PLT","ROL","REL ,BUCKET/PAIL","TUBE","CYLINDER","BALE","BUCKET","BAG","DRUMS","BOX","TOTE","PIECES","CASE","BUNDLE","CARTON","COIL","OTHER","PAIL","CAN","CRATE","GAYLORD","PALLET","ROLL","REEL"] contains(inObj))
inObj
else
"Other"
---
{
	ns0#quoteSpeedFreightShipment: {
		ns0#freightShipmentQuoteRequest: {
			ns0#senderCity: vars.wexnetDetails.senderCity,
			ns0#senderState: vars.wexnetDetails.senderState,
			ns0#senderZip: vars.wexnetDetails.senderZip,
			ns0#senderCountryCode: vars.wexnetDetails.senderCountryCode,
			ns0#insidePickup: vars.wexnetDetails.insidePickup,
			ns0#liftgatePickup: vars.wexnetDetails.liftgatePickup,
			ns0#residentialPickup: vars.wexnetDetails.residentialPickup,
			ns0#tradeshowPickup: vars.wexnetDetails.tradeshowPickup,
			ns0#constructionSitePickup: vars.wexnetDetails.constructionSitePickup,
			ns0#limitedAccessPickup: vars.wexnetDetails.limitedAccessPickup,
			ns0#limitedAccessPickupType: vars.wexnetDetails.limitedAccessPickupType,
			ns0#receiverCity: vars.wexnetDetails.receiverCity,
			ns0#receiverState: vars.wexnetDetails.receiverState,
			ns0#receiverZip: vars.wexnetDetails.receiverZip,
			ns0#receiverCountryCode: vars.wexnetDetails.receiverCountryCode,
			ns0#insideDelivery: vars.wexnetDetails.insideDelivery,
			ns0#liftgateDelivery: vars.wexnetDetails.liftgateDelivery,
			ns0#residentialDelivery: vars.wexnetDetails.residentialDelivery,
			ns0#tradeshowDelivery: vars.wexnetDetails.tradeshowDelivery,
			ns0#constructionSiteDelivery: vars.wexnetDetails.constructionSiteDelivery,
			ns0#limitedAccessDelivery: vars.wexnetDetails.limitedAccessDelivery,
			ns0#limitedAccessDeliveryType: vars.wexnetDetails.limitedAccessDeliveryType,
			ns0#notifyBeforeDelivery: vars.wexnetDetails.notifyBeforeDelivery,
			ns0#commdityDetails: {
				ns0#handlingUnitDetails: {
					ns0#wsHandlingUnit: vars.wexnetDetails.commdityDetails.handlingUnitDetails.wsHandlingUnit map ( item , index ) ->{
						ns0#typeOfHandlingUnit: checkTypeOfHU(item.typeOfHandlingUnit),
						ns0#numberOfHandlingUnit: item.numberOfHandlingUnit,
						ns0#handlingUnitHeight: item.handlingUnitHeight,
						ns0#handlingUnitLength: item.handlingUnitLength,
						ns0#handlingUnitWidth: item.handlingUnitWidth,
						ns0#lineItemDetails: item.lineItemDetails.wsLineItem map (lineItem, index) -> {
							ns0#wsLineItem: {
								ns0#lineItemClass: lineItem.lineItemClass,
								ns0#lineItemWeight: lineItem.lineItemWeight,
								ns0#lineItemDescription: "Test Description",
								ns0#lineItemPieceType: checkLineItemPieceType(lineItem.lineItemPieceType),
								ns0#piecesOfLineItem: lineItem.piecesOfLineItem,
								ns0#isHazmatLineItem: lineItem.isHazmatLineItem,
								ns0#lineItemHazmatInfo: {
									ns0#lineItemHazmatUNNumberHeader: lineItem.lineItemHazmatInfo.lineItemHazmatUNNumberHeader,
									ns0#lineItemHazmatUNNumber: lineItem.lineItemHazmatInfo.lineItemHazmatUNNumber,
									ns0#lineItemHazmatClass: lineItem.lineItemHazmatInfo.lineItemHazmatClass,
									ns0#lineItemHazmatEmContactPhone: "0000000000",
									ns0#lineItemHazmatPackagingGroup: lineItem.lineItemHazmatInfo.lineItemHazmatPackagingGroup
								}
							}
						}
					}
				}
			}
		}
	}
}